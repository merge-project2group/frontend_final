import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Promotion } from '@/types/promotion'
import { useLoadingStore } from '@/stores/loading'
import promotionService from '@/services/promotion'

export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const promotions = ref<Promotion[]>([])
  const initialPromotion: Promotion = {
    name: '',
    discount: 0,
    startdate: '',
    enddate: '',
    status: [],
    type: []
  }
  const editedPromotion = ref<Promotion>(JSON.parse(JSON.stringify(initialPromotion)))

  async function getPromotion(id: number) {
    loadingStore.doLoad()
    const res = await promotionService.getPromotion(id)
    editedPromotion.value = res.data
    loadingStore.finish()
  }
  async function getPromotions() {
    loadingStore.doLoad()
    const res = await promotionService.getPromotions()
    promotions.value = res.data
    loadingStore.finish()
  }
  async function savePromotion() {
    const promotion = editedPromotion.value
    loadingStore.doLoad()
    if (!promotion.id) {
      console.log('Post' + JSON.stringify(promotion))
      const res = await promotionService.addNew(promotion)
    } else {
      console.log('Patch' + JSON.stringify(promotion))
      const res = await promotionService.update(promotion)
    }
    await getPromotions()
    loadingStore.finish()
  }

  async function deletePromotion() {
    loadingStore.doLoad()
    const promotion = editedPromotion.value
    const res = await promotionService.delPromotion(promotion)
    await getPromotions()
    loadingStore.finish()
  }

  function clearForm() {
    editedPromotion.value = JSON.parse(JSON.stringify(initialPromotion))
  }
  return {
    promotions,
    getPromotions,
    savePromotion,
    deletePromotion,
    getPromotion,
    editedPromotion,
    clearForm
  }
})
