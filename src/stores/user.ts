import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import type { User } from '@/types/User'
import { useMessageStore } from './message'
import user from '@/services/user'

export const useUserStore = defineStore('user', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const users = ref<User[]>([])
  const currentTime = ref<string>('')
  const dataRatesalary = ref()
  const datalowsalary = ref()
  const datareportcheckwork = ref()
  const dataCountNameByBranchId = ref()
  const datagetUserbybranchid = ref()
  const initialUser: User & { files: File[] } = {
    email: '',
    password: '',
    fullName: '',
    gender: 'male',
    roles: [{ id: 2, name: 'user' }],
    image: 'noimage.jpg',
    files: [],
    tel: '',
    salary: 0,
    position: 'Staff',
    branch: '0'
  }

  const editedUser = ref<User & { files: File[] }>(JSON.parse(JSON.stringify(initialUser)))

  async function getUser(id: number) {
    try {
      loadingStore.doLoad()
      const res = await userService.getUser(id)
      editedUser.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('eror')
      loadingStore.finish()
    }
  }
  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }

  async function getreportcheckwork(id: number) {
    const res = await userService.getreportcheckwork(id)
    datareportcheckwork.value = res.data
  }

  async function getLowsalary() {
    const res = await userService.getLowsalary()
    console.log(res)
    datalowsalary.value = res.data
  }

  async function getCountNameByBranchId(id: number) {
    //console.log(id);

    const res = await userService.getCountNameByBranchId(id)
    dataCountNameByBranchId.value = res.data
    console.log(',s', dataCountNameByBranchId)
  }

  async function getUseridbybranchid(id: number) {
    const res = await userService.getUseridbybranchid(id)
    console.log(res)
    datagetUserbybranchid.value = res.data
  }

  async function getRateSalary(min: number, max: number, idbranch: number) {
    const res = await userService.getRateSalary(min, max, idbranch)
    dataRatesalary.value = res.data[0]
  }

  async function saveUser() {
    try {
      loadingStore.doLoad()
      const user = editedUser.value
      if (!user.id) {
        // Add new
        console.log('Post ' + JSON.stringify(user))
        const res = await userService.addUser(user)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(user))
        const res = await userService.updateUser(user)
      }
      await getUsers()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteUser() {
    try {
      loadingStore.doLoad()
      const user = editedUser.value
      const res = await userService.delUser(user)
      await getUsers()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedUser.value = JSON.parse(JSON.stringify(initialUser))
  }

  return {
    users,
    getUsers,
    saveUser,
    deleteUser,
    editedUser,
    getUser,
    clearForm,
    getLowsalary,
    getRateSalary,
    dataRatesalary,
    datalowsalary,
    getUseridbybranchid,
    datagetUserbybranchid,
    getreportcheckwork,
    datareportcheckwork,
    getCountNameByBranchId,
    dataCountNameByBranchId
  }
})
