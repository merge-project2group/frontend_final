import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import checkworkService from '@/services/checkwork'
import type { Checkwork } from '@/types/CheckWork'
import { useMessageStore } from './message'

export const useCheckworkStore = defineStore('checkwork', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const checkworks = ref<Checkwork[]>([])
  const datagetfindbybranchid = ref()
  const initialCheckwork: Checkwork = {
    id: 0,
    fullname: '',
    date: '',
    timein: '',
    timeout: '',
    status: ''
  }
  const editedCheckWork = ref<Checkwork & { files: File[] }>(
    JSON.parse(JSON.stringify(initialCheckwork))
  )

  async function getCheckWork(id: number) {
    try {
      loadingStore.doLoad()
      const res = await checkworkService.getCheckwork(id)
      editedCheckWork.value = res.data
      loadingStore.finish()
    } catch (e) {
      console.log('eror')
      loadingStore.finish()
    }
  }
  async function getCheckworks() {
    loadingStore.doLoad()
    const res = await checkworkService.getCheckworks()
    checkworks.value = res.data
    loadingStore.finish()
  }

  async function getfindbybranchid(id: number) {
    console.log(id)

    const res = await checkworkService.findbybranchid(id)
    checkworks.value = res.data
  }

  async function saveCheckwork() {
    try {
      loadingStore.doLoad()
      const checkwork = editedCheckWork.value
      if (!checkwork.id) {
        // Add new
        console.log('Post ' + JSON.stringify(checkwork))
        const res = await checkworkService.addCheckwork(checkwork)
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(checkwork))
        const res = await checkworkService.updateCheckwork(checkwork)
      }

      await getCheckworks()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function deleteCheckwork() {
    try {
      loadingStore.doLoad()
      const checkwork = editedCheckWork.value
      const res = await checkworkService.delCheckwork(checkwork)
      await getCheckworks()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedCheckWork.value = JSON.parse(JSON.stringify(initialCheckwork))
  }
  return {
    checkworks,
    getCheckworks,
    saveCheckwork,
    deleteCheckwork,
    editedCheckWork,
    getCheckWork,
    clearForm,
    getfindbybranchid,
    datagetfindbybranchid
  }
})
