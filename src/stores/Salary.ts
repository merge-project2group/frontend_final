import { ref, computed, nextTick, onMounted, type Ref, watchEffect } from 'vue'
import { defineStore } from 'pinia'
import salaryService from '@/services/salaryService'
import type { Salary } from '@/types/Salary'
import { useLoadingStore } from '@/stores/loading'
import type { Branch } from '@/types/Branch'
import { useBranchStore } from './branch'
import type { User } from '@/types/User'
import { useUserStore } from './user'

const userStore = useUserStore()
const branchStore = useBranchStore()
const loadingStore = useLoadingStore()
const loading = ref(false)
const paymentDialog1 = ref(false)
const paymentDialog2 = ref(false)
const salarys = ref<Salary[]>([])
const Branchs = ref<Branch[]>([])
const users = ref<User[]>([])
const datasumsalary = ref()
const datagetfindPieYear = ref()
const currentTime = ref<string>('')
const isPaid = (item: Salary) => {
  return item.status === 'Paid'
}

export const useSalaryStore = defineStore('salary', () => {
  const initilSalary: Salary = {
    date: currentTime.value,
    userid: 0,
    fullname: '',
    workinghour: 0,
    workrate: 0,
    status: '',
    typepay: '',
    salary: 0,
    isChecked: false
  }
  const editedSalary = ref<Salary>(JSON.parse(JSON.stringify(initilSalary)))

  async function getSalarys() {
    loadingStore.doLoad()
    const res = await salaryService.getSalarys()
    salarys.value = res.data
    loadingStore.finish()
  }

  async function getsumsalary(id: number) {
    datasumsalary.value = id
  }

  async function getSalary(id: number) {
    loadingStore.doLoad()
    const res = await salaryService.getSalary(id)
    editedSalary.value = res.data
    loadingStore.finish()
  }

  async function getfindPieYear(id: number) {
    const res = await salaryService.getfindPieYear(id)
    datagetfindPieYear.value = res.data
  }

  async function saveSalary() {
    const Salary = editedSalary.value
    loadingStore.doLoad()
    if (!Salary.id) {
      //Add new
      editedSalary.value.salary = editedSalary.value.workinghour * editedSalary.value.workrate
      Salary.date = getCurrentTime()
      const res = await salaryService.addSalary(Salary)
    } else {
      //Update
      const res = await salaryService.updateSalary(Salary)
    }
    await getSalarys()
    loadingStore.finish()
  }

  async function deleteSalary() {
    loadingStore.doLoad()
    const res = await salaryService.delSalary(editedSalary.value)
    await getSalarys()
    loadingStore.finish()
  }

  function clearForm() {
    editedSalary.value = JSON.parse(JSON.stringify(initilSalary))
  }

  async function pay(item: Salary) {
    loadingStore.doLoad()
    item.status = 'Paid'
    await salaryService.updateSalary(item)
    await getSalarys()
    paymentDialog1.value = true
    loadingStore.finish()
  }

  const salarysPaid = computed(() => {
    return salarys.value.filter((s) => s.status === 'Paid')
  })

  const selectedFilter = ref('all')

  const filteredSalarys = computed(() => {
    if (selectedFilter.value === 'paid') {
      return salarys.value.filter((salary) => salary.status === 'Paid')
    } else if (selectedFilter.value === 'unpaid') {
      return salarys.value.filter((salary) => salary.status !== 'Paid')
    } else {
      return salarys.value
    }
  })

  watchEffect(() => {
    if (
      selectedFilter.value === 'paid' ||
      selectedFilter.value === 'unpaid' ||
      selectedFilter.value === 'all'
    ) {
      getSalarys()
    }
  })

  async function cancelPayment(item: Salary) {
    loadingStore.doLoad()
    if (isPaid(item)) {
      // เรียกใช้งาน isPaid ฟังก์ชันเพื่อตรวจสอบว่ารายการนี้ได้รับการจ่ายเงินแล้วหรือไม่
      item.status = 'Unpaid' // เปลี่ยนสถานะเป็นยังไม่ได้รับการจ่ายเงิน
      await salaryService.updateSalary(item) // อัปเดตข้อมูลในฐานข้อมูล
      loadingStore.finish()
      await getSalarys() // โหลดข้อมูลเงินเดือนใหม่เพื่ออัปเดตหน้าประวัติ
      paymentDialog2.value = true // แสดงกล่องข้อความยืนยันการยกเลิกรายการ
    } else {
      // กรณีที่รายการเงินเดือนยังไม่ได้รับการจ่ายเงิน ไม่ต้องทำอะไร
      loadingStore.finish()
    }
  }

  async function handleClosePaymentDialog(item: Salary) {
    loadingStore.finish()
    paymentDialog1.value = false
    paymentDialog2.value = false
    await getSalarys()
  }

  function getCurrentTime() {
    const now = new Date()
    const year = now.getFullYear()
    const month = (now.getMonth() + 1).toString().padStart(2, '0')
    const day = now.getDate().toString().padStart(2, '0')
    return `${year}-${month}-${day}`
  }

  const branchId = computed(() => {
    const uniqueId = new Set<number>()
    Branchs.value.forEach((branch) => {
      if (branch.id !== undefined) {
        uniqueId.add(branch.id)
      }
    })
    return Array.from(uniqueId)
  })
  branchStore.getBranchs()

  const userId = computed(() => {
    const uniqueId = new Set<number>()
    users.value.forEach((user) => {
      if (user.id !== undefined) {
        uniqueId.add(user.id)
      }
    })
    return Array.from(uniqueId)
  })
  userStore.getUsers()

  onMounted(() => {
    currentTime.value = getCurrentTime()
  })

  return {
    salarys,
    salarysPaid,
    cancelPayment,
    filteredSalarys,
    selectedFilter,
    getSalarys,
    saveSalary,
    deleteSalary,
    clearForm,
    pay,
    currentTime,
    handleClosePaymentDialog,
    getCurrentTime,
    editedSalary,
    getSalary,
    paymentDialog1,
    paymentDialog2,
    branchId,
    userId,
    datasumsalary,
    getsumsalary,
    datagetfindPieYear,
    getfindPieYear
  }
})
