import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import BranchService from '@/services/BranchService'
import type { Branch } from '@/types/Branch'
import { useLoadingStore } from './loading'

const loadingStore = useLoadingStore()
const Branchs = ref<Branch[]>([])
export const useBranchStore = defineStore('branch', () => {
  const initilBranch: Branch = {
    name: '',
    tel: '',
    address: ''
  }
  const editedBranch = ref<Branch>(JSON.parse(JSON.stringify(initilBranch)))

  async function getBranchs() {
    loadingStore.doLoad()
    const res = await BranchService.getBranchs()
    Branchs.value = res.data
    loadingStore.finish()
  }

  async function getBranch(id: number) {
    loadingStore.doLoad()
    const res = await BranchService.getBranch(id)
    editedBranch.value = res.data
    loadingStore.finish()
  }

  async function saveBranch() {
    const Branch = editedBranch.value
    loadingStore.doLoad()
    if (!Branch.id) {
      //Add new
      const res = await BranchService.addBranch(Branch)
    } else {
      //Update
      const res = await BranchService.updateBranch(Branch)
    }
    await getBranchs()
    loadingStore.finish()
  }

  async function deleteBranch() {
    loadingStore.doLoad()
    const res = await BranchService.delBranch(editedBranch.value)
    await getBranchs()
    loadingStore.finish()
  }

  function clearForm() {
    editedBranch.value = JSON.parse(JSON.stringify(initilBranch))
  }

  const branchId = computed(() => {
    const uniqueId = new Set<number>()
    Branchs.value.forEach((branch) => {
      if (branch.id !== undefined) {
        uniqueId.add(branch.id)
      }
    })
    return Array.from(uniqueId)
  })
  getBranchs()

  return {
    Branchs,
    getBranchs,
    saveBranch,
    deleteBranch,
    clearForm,
    editedBranch,
    getBranch,
    branchId
  }
})
