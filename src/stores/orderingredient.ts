import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Ingredient } from '@/types/Ingredient'
import { useMessageStore } from './message'
import { useAuthStore } from './auth'
import orderingredientService from '@/services/orderingredient'
import type { OrderIngredient } from '@/types/OrderIngredient'

export const useOrderIngredientStore = defineStore('orderingredients', () => {
  const loadingStore = useLoadingStore()
  const authStore = useAuthStore()
  const messageStore = useMessageStore()
  const orderingredients = ref<OrderIngredient[]>([])
  const SaveOrderIngredient = ref<OrderIngredient[]>([])
  const dataCalculas = ref()

  const formatDate = (date: Date): string => {
    const day = String(date.getDate()).padStart(2, '0')
    const month = String(date.getMonth() + 1).padStart(2, '0')
    const year = date.getFullYear()

    return `${year}-${day}-${month}`
  }

  const initialOrderIngredient: OrderIngredient & { files: File[] } = {
    id: 0,
    name: '',
    price: 0,
    amount: 0,
    Savedate: formatDate(new Date()),
    branch: ''
  }
  const editedOrderIngredient = ref<OrderIngredient & { files: File[] }>(
    JSON.parse(JSON.stringify(initialOrderIngredient))
  )

  async function getOrderIngredient(id: number) {
    try {
      loadingStore.doLoad()
      const res = await orderingredientService.getOrderIngredient(id)
      editedOrderIngredient.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function getOrderIngredients() {
    loadingStore.doLoad()
    const res = await orderingredientService.getOrderIngredients()
    orderingredients.value = res.data
    loadingStore.finish()
  }

  async function saveOrderIngredient() {
    try {
      loadingStore.doLoad()
      editedOrderIngredient.value.Savedate = formatDate(new Date())
      if (authStore.getCurrentUser()?.branch != null) {
        editedOrderIngredient.value.branch = authStore.getCurrentUser().branch
      }
      const orderingredient = editedOrderIngredient.value
      if (!orderingredient.id) {
        // Add new
        console.log('Post ' + JSON.stringify(orderingredient))
        const res = await orderingredientService.addOrderIngredient(orderingredient)
        console.log('after')
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(orderingredient))
        const res = await orderingredientService.updateOrderIngredient(orderingredient)
      }
      await getOrderIngredients()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function deleteOrderIngredient() {
    try {
      loadingStore.doLoad()
      const ingredient = editedOrderIngredient.value
      const res = await orderingredientService.delOrderIngredient(ingredient)
      await getOrderIngredients()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function clearForm() {
    editedOrderIngredient.value = JSON.parse(JSON.stringify(initialOrderIngredient))
  }

  return {
    editedOrderIngredient,
    getOrderIngredient,
    orderingredients,
    getOrderIngredients,
    saveOrderIngredient,
    deleteOrderIngredient,
    clearForm
  }
})
