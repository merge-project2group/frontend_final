import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import orderService from '@/services/order'
import { useMessageStore } from './message'
import { useLoadingStore } from './loading'

export const useReceiptStore = defineStore('receipt', () => {
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const authStore = useAuthStore()
  const receiptItems = ref<ReceiptItem[]>([])
  const receipt = ref<Receipt>()
  const datagetfindsales_summary_max = ref()
  const datagetfindsales_summary_min = ref()
  const statusPayment = ref(false)
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      amount: 0,
      change: 0,
      paymentType: '',
      userId: authStore.getCurrentUser()!.id!,
      memberId: -1,
      user: authStore.getCurrentUser()!
    }
    receiptItems.value = []
  }
  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  const calReceipt = function () {
    receipt.value!.total = 0
    receipt.value!.amount = 0
    for (let i = 0; i < receiptItems.value.length; i++) {
      receipt.value!.total += receiptItems.value[i].price * receiptItems.value[i].unit
      receipt.value!.amount += receiptItems.value[i].unit
    }
  }

  async function getfindsales_summary_max() {
    try {
      const res = await orderService.getfindsales_summary_max()
      datagetfindsales_summary_max.value = res.data
      console.log('saaaaas')
    } catch (error) {
      console.log('dd')
    }
  }

  async function getfindsales_summary_min() {
    try {
      const res = await orderService.getfindsales_summary_min()
      datagetfindsales_summary_min.value = res.data
      console.log('saaaaas')
    } catch (error) {
      console.log('dd')
    }
  }

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    const existingItem = receiptItems.value.find(
      (item) => item.productId === newReceiptItem.productId
    )
    if (existingItem) {
      existingItem.unit += 1
    } else {
      receiptItems.value.push(newReceiptItem)
    }
  }

  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      deleteReceiptItem(selectedItem)
    }
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
  }
  const order = async () => {
    console.log('receipt Store')

    try {
      loadingStore.doLoad()
      await orderService.addOrder(receipt.value!, receiptItems.value)
      initReceipt()
      loadingStore.finish()
      statusPayment.value = true
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  async function getReceipts() {
    loadingStore.doLoad()
    const res = await orderService.getReceipts()
    receipt.value = res.data
    loadingStore.finish()
  }

  return {
    receipt,
    receiptItems,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem,
    order,
    statusPayment,
    getfindsales_summary_max,
    datagetfindsales_summary_max,
    getfindsales_summary_min,
    datagetfindsales_summary_min,
    getReceipts
  }
})
