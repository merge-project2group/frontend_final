import { useLoadingStore } from './loading'
import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import ingredientService from '@/services/ingredient'
import type { Ingredient } from '@/types/Ingredient'
import { useMessageStore } from './message'
import ingredient from '@/services/ingredient'
import { useAuthStore } from './auth'

export const useIngredientStore = defineStore('ingredient', () => {
  const loadingStore = useLoadingStore()
  const authStore = useAuthStore()
  const messageStore = useMessageStore()
  const ingredients = ref<Ingredient[]>([])
  const SaveIngredient = ref<Ingredient[]>([])
  const dataCalculas = ref()

  function formatDate(date: Date) {
    const year = date.getFullYear()
    const month = (date.getMonth() + 1).toString().padStart(2, '0')
    const day = date.getDate().toString().padStart(2, '0')
    return `${year}-${month}-${day}`
  }

  const initialIngredient: Ingredient & { files: File[] } = {
    name: '',
    price: 0,
    image: 'noimage.jpg',
    amount: 0,
    files: [],
    Savedate: formatDate(new Date()),
    used: 0,
    branch: 1
  }
  const editedIngredient = ref<Ingredient & { files: File[] }>(
    JSON.parse(JSON.stringify(initialIngredient))
  )

  async function getIngredient(id: number) {
    try {
      loadingStore.doLoad()
      const res = await ingredientService.getIngredient(id)
      editedIngredient.value = res.data
      loadingStore.finish()
    } catch (e: any) {
      console.log('eror')
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function getIngredients() {
    loadingStore.doLoad()
    const res = await ingredientService.getIngredients()
    ingredients.value = res.data
    loadingStore.finish()
  }

  async function getCalculateIngredientTotal() {
    const res = await ingredientService.getCalculateIngredientTotal()
    console.log(res)
    dataCalculas.value = res
  }

  async function saveIngredient() {
    try {
      loadingStore.doLoad()
      editedIngredient.value.Savedate = formatDate(new Date())
      if (authStore.getCurrentUser()?.branch != null) {
        editedIngredient.value.branch = authStore.getCurrentUser()?.branch.id
      }
      const ingredient = editedIngredient.value
      if (!ingredient.id) {
        // Add new
        console.log('Post ' + JSON.stringify(ingredient))
        const res = await ingredientService.addIngredient(ingredient)
        console.log('after')
      } else {
        // Update
        console.log('Patch ' + JSON.stringify(ingredient))
        const res = await ingredientService.updateIngredient(ingredient)
      }
      await getIngredients()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }
  async function deleteIngredient() {
    try {
      loadingStore.doLoad()
      const ingredient = editedIngredient.value
      const res = await ingredientService.delIngredient(ingredient)
      await getIngredients()
      loadingStore.finish()
    } catch (e: any) {
      messageStore.showMessage(e.message)
      loadingStore.finish()
    }
  }

  function copyIngredients() {
    console.log('Copy Ingredient')
    const currentDate = new Date()

    const formattedCurrentDate = formatDate(currentDate)

    const nextDay = new Date(currentDate)
    nextDay.setDate(nextDay.getDate() + 1)

    for (const i of ingredients.value) {
      if (i.Savedate == formatDate(nextDay)) {
        console.log(i.Savedate)
        console.log(formatDate(nextDay))
        console.log('Can save 1 time in 1 day!!!')
      }
    }

    const copiedIngredients = ingredients.value
      .filter((ingredient) => ingredient.Savedate === formattedCurrentDate)
      .map((ingredient) => ({
        id: undefined,
        name: ingredient.name,
        price: ingredient.price,
        amount: ingredient.amount,
        image: ingredient.image,
        used: 0,
        Savedate: formatDate(nextDay), // กำหนดวันที่และเวลาปัจจุบัน
        files: []
      }))

    const today = ingredients.value.filter(
      (ingredient) => ingredient.Savedate == formattedCurrentDate
    )

    for (const i of copiedIngredients) {
      for (const j of today) {
        if (i.name == j.name) {
          i.amount -= j.used
        }
      }
    }

    ingredients.value.push(...copiedIngredients)

    // บันทึกข้อมูลที่คัดลอกลงในฐานข้อมูล
    copiedIngredients.forEach(async (ingredient) => {
      try {
        console.log('save to db')
        loadingStore.doLoad()
        const res = await ingredientService.addIngredient(ingredient)
        console.log()
        await getIngredients() // อัปเดตข้อมูลหลังจากบันทึกข้อมูลใหม่
        loadingStore.finish()
      } catch (e: any) {
        messageStore.showMessage(e.message)
        loadingStore.finish()
      }
    })
  }

  function clearForm() {
    editedIngredient.value = JSON.parse(JSON.stringify(initialIngredient))
  }

  return {
    ingredients,
    SaveIngredient,
    getIngredients,
    saveIngredient,
    deleteIngredient,
    editedIngredient,
    getIngredient,
    clearForm,
    copyIngredients,
    getCalculateIngredientTotal,
    dataCalculas
  }
})
