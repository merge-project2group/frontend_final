import { ref, computed, nextTick, onMounted, type Ref, watchEffect } from 'vue'
import { defineStore } from 'pinia'
import billService from '@/services/billService'
import type { Bill } from '@/types/Bill'
import { useLoadingStore } from '@/stores/loading'

const loadingStore = useLoadingStore()
const loading = ref(false)
const paymentDialog1 = ref(false)
const paymentDialog2 = ref(false)
const bills = ref<Bill[]>([])
const isPaid = (item: Bill) => {
  return item.timepay !== 'null'
}
const currentTime = ref<string>('')

export const useBillStore = defineStore('bill', () => {
  const initilBill: Bill = {
    invoice: 'P',
    namepd: '',
    amountbill: 0,
    units: '',
    priceperunits: 0,
    discount: 0,
    price: 0,
    time: currentTime.value,
    timepay: 'null',
    status: '',
    isChecked: false
  }
  const editedBill = ref<Bill>(JSON.parse(JSON.stringify(initilBill)))

  async function getBills() {
    loadingStore.doLoad()
    const res = await billService.getBills()
    bills.value = res.data
    loadingStore.finish()
  }

  async function getBill(id: number) {
    loadingStore.doLoad()
    const res = await billService.getBill(id)
    editedBill.value = res.data
    loadingStore.finish()
  }

  async function saveBill() {
    const Bill = editedBill.value
    loadingStore.doLoad()
    if (!Bill.id) {
      //Add new
      editedBill.value.price =
        editedBill.value.amountbill * editedBill.value.priceperunits - editedBill.value.discount
      Bill.time = getCurrentTime()
      const res = await billService.addBill(Bill)
    } else {
      //Update
      Bill.price = Bill.amountbill * Bill.priceperunits - Bill.discount
      const res = await billService.updateBill(Bill)
    }
    await getBills()
    loadingStore.finish()
  }

  async function deleteBill() {
    loadingStore.doLoad()
    const res = await billService.delBill(editedBill.value)
    await getBills()
    loadingStore.finish()
  }

  function clearForm() {
    editedBill.value = JSON.parse(JSON.stringify(initilBill))
  }

  async function pay(item: Bill) {
    loadingStore.doLoad()
    if (!item.timepay || item.timepay === 'null') {
      item.timepay = getCurrentTime()
      item.status = 'Paid'
      await billService.updateBill(item)
      paymentDialog1.value = true
      loadingStore.finish()
    }
  }

  async function cancelPayment(item: Bill) {
    loadingStore.doLoad()
    if (isPaid(item)) {
      item.timepay = 'null'
      item.status = 'Un-Paid'
      item.isChecked = false
      await billService.updateBill(item)
      paymentDialog2.value = true
      loadingStore.finish()
    }
  }
  async function handleClosePaymentDialog(item: Bill) {
    loadingStore.finish()
    paymentDialog1.value = false
    paymentDialog2.value = false
    item.isChecked = true
    await getBills()
  }

  function getCurrentTime() {
    const now = new Date()
    const year = now.getFullYear()
    const month = (now.getMonth() + 1).toString().padStart(2, '0')
    const day = now.getDate().toString().padStart(2, '0')
    return `${year}-${month}-${day}`
  }

  function selectUnit() {
    switch (editedBill.value.namepd) {
      case 'Water':
        editedBill.value.units = 'Water'
        break
      case 'Electricity':
        editedBill.value.units = 'Electricity'
        break
      case 'Rent':
        editedBill.value.units = 'Baht'
        break
      default:
        editedBill.value.units = ''
        break
    }
  }
  watchEffect(() => {
    selectUnit()
  })

  const selectedFilter = ref('all')

  // computed property ของ bills ที่ถูกกรองตาม selectedFilter
  const filteredBills = computed(() => {
    if (selectedFilter.value === 'Water') {
      return bills.value.filter((bill) => bill.units === 'Water')
    } else if (selectedFilter.value === 'Electricity') {
      return bills.value.filter((bill) => bill.units === 'Electricity')
    } else if (selectedFilter.value === 'Rent') {
      return bills.value.filter((bill) => bill.units === 'Baht')
    } else {
      return bills.value
    }
  })

  const filteredBills1 = computed(() => {
    return bills.value.filter((bill) => bill.timepay !== 'null')
  })

  watchEffect(() => {
    if (
      selectedFilter.value === 'all' ||
      selectedFilter.value === 'Water' ||
      selectedFilter.value === 'Electricity' ||
      selectedFilter.value === 'Rent'
    ) {
      getBills()
    }
  })

  onMounted(() => {
    currentTime.value = getCurrentTime()
  })

  return {
    bills,
    getBills,
    saveBill,
    deleteBill,
    clearForm,
    currentTime,
    getCurrentTime,
    editedBill,
    getBill,
    pay,
    cancelPayment,
    paymentDialog1,
    paymentDialog2,
    handleClosePaymentDialog,
    selectUnit,
    selectedFilter,
    filteredBills,
    selectFilter(type: string) {
      selectedFilter.value = type
    },
    filteredBills1
  }
})
