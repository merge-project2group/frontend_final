type Type = 'bath' | '%'
type Promotion = {
  id?: number
  name: string
  discount: number
  startdate: string
  enddate: string
  status: Type[]
  type: Type[]
}

export type { Promotion, Type }
