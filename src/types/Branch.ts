type Branch = {
  id?: number
  name: string
  tel: string
  address: string
}

export type { Branch }
