import type { Role } from './Role'
import type { Salary } from './Salary'
type Gender = 'male' | 'female' | 'others'
type Position = 'Manager' | 'Staff' | 'Owner'
type User = {
  id?: number
  email: string
  password: string
  fullName: string
  gender: Gender // Male, Female, Others
  image: string
  roles: Role[] // admin, user
  tel: string
  salary: number
  position: Position
  branch: string
  salarys: Salary[]
}
export type { Gender, User, Position }
