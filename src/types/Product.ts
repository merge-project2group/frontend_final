import type { Type } from './Type'

type Product = {
  id?: number
  name: string
  price: number
  type: Type[]
  image: string
}
function getImageUrl(product: Product) {
  return `/img/coffees/product${product.id}.png`
}
export { type Product, getImageUrl }
