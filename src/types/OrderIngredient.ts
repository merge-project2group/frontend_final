type OrderIngredient = {
  id?: number
  name: string
  price: number
  amount: number
  image: string
  Savedate: string
  branch: string
}
export { type OrderIngredient }
