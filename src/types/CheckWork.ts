type Checkwork = {
  id: number
  fullname: string
  date: string
  timein: string
  timeout: string
  status: string
}

export { type Checkwork }
