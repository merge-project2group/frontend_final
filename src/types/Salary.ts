type Salary = {
  id?: number
  date: string
  userid: number
  fullname: string
  workinghour: number
  workrate: number
  typepay: string
  status: string
  salary: number
  isChecked: boolean
}

export type { Salary }
