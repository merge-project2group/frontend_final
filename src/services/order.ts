import type { Receipt } from '@/types/Receipt'
import http from './http'
import type { ReceiptItem } from '@/types/ReceiptItem'
type ReceiptDto = {
  orderItems: {
    productId: number
    qty: number
  }[]
  userId: number
}
function addOrder(receipt: Receipt, receiptItems: ReceiptItem[]) {
  const ReceiptDto: ReceiptDto = {
    orderItems: [],
    userId: 0
  }
  ReceiptDto.userId = receipt.userId
  ReceiptDto.orderItems = receiptItems.map((item) => {
    return {
      productId: item.productId,
      qty: item.unit
    }
  })
  console.log('receipt Service')

  return http.post('/orders', ReceiptDto)
}

function getfindsales_summary_max() {
  return http.get(`/orders/findsales_summary_max`)
}

function getfindsales_summary_min() {
  return http.get(`/orders/findsales_summary_min`)
}

function getReceipts() {
  return http.get('/orders')
}

export default { addOrder, getfindsales_summary_max, getfindsales_summary_min, getReceipts }
