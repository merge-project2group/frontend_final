import type { Checkwork } from '@/types/CheckWork'
import http from './http'

function addCheckwork(checkwork: Checkwork & { files: File[] }) {
  const formData = new FormData()
  formData.append('fullname', checkwork.fullname)
  formData.append('date', checkwork.date)
  formData.append('timein', checkwork.timein)
  formData.append('timeout', checkwork.timeout)
  formData.append('status', checkwork.status)
  if (checkwork.files && checkwork.files.length > 0) formData.append('file', checkwork.files[0])
  return http.post('/checkworks', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateCheckwork(checkwork: Checkwork & { files: File[] }) {
  const formData = new FormData()
  formData.append('fullname', checkwork.fullname)
  formData.append('date', checkwork.date)
  formData.append('timein', checkwork.timein)
  formData.append('timeout', checkwork.timeout)
  formData.append('status', checkwork.status)
  if (checkwork.files && checkwork.files.length > 0) formData.append('file', checkwork.files[0])
  return http.post(`/checkworks/${checkwork.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delCheckwork(checkwork: Checkwork) {
  return http.delete(`/checkworks/${checkwork.id}`)
}

function getCheckwork(id: number) {
  return http.get(`/checkworks/${id}`)
}

function findbybranchid(id: number) {
  console.log(id)
  return http.get(`/checkworks/indbybranchid/${id}`)
}

function getCheckworks() {
  return http.get('/checkworks')
}

export default {
  addCheckwork,
  updateCheckwork,
  delCheckwork,
  getCheckwork,
  getCheckworks,
  findbybranchid
}
