import type { Promotion } from '@/types/promotion'
import http from './http'

function addNew(promotion: Promotion) {
  return http.post('/promotion', promotion)
}

function update(promotion: Promotion) {
  return http.patch(`/promotion/${promotion.id}`, promotion)
}

function delPromotion(promotion: Promotion) {
  return http.delete(`/promotion/${promotion.id}`)
}

function getPromotion(id: number) {
  return http.get(`/promotion/${id}`)
}

function getPromotions() {
  return http.get('/promotion')
}

export default { addNew, update, delPromotion, getPromotion, getPromotions }
