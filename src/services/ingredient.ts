import type { Ingredient } from '@/types/Ingredient'
import http from './http'

function formatDate(date: Date) {
  const year = date.getFullYear()
  const month = (date.getMonth() + 1).toString().padStart(2, '0')
  const day = date.getDate().toString().padStart(2, '0')
  return `${year}-${month}-${day}`
}

function addIngredient(ingredient: Ingredient & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', ingredient.name)
  formData.append('price', ingredient.price.toString())
  formData.append('amount', ingredient.amount.toString())
  formData.append('Savedate', ingredient.Savedate)
  formData.append('used', ingredient.used.toString())
  // formData.append('branch', ingredient.branch.toString())
  if (ingredient.files && ingredient.files.length > 0) formData.append('file', ingredient.files[0])
  return http.post('/ingredients', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateIngredient(ingredient: Ingredient & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', ingredient.name)
  formData.append('price', ingredient.price.toString())
  formData.append('amount', ingredient.amount.toString())
  formData.append('used', ingredient.used.toString())
  if (ingredient.files && ingredient.files.length > 0) formData.append('file', ingredient.files[0])
  return http.post(`/ingredients/${ingredient.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delIngredient(ingredient: Ingredient) {
  return http.delete(`/ingredients/${ingredient.id}`)
}

function getCalculateIngredientTotal() {
  return http.get(`/ingredients/ingredients/CalculateIngredientTotal`)
}

function getIngredient(id: number) {
  return http.get(`/ingredients/${id}`)
}

function getIngredients() {
  return http.get('/ingredients')
}

export default {
  addIngredient,
  updateIngredient,
  delIngredient,
  getIngredient,
  getIngredients,
  getCalculateIngredientTotal
}
