import type { User } from '@/types/User'
import http from './http'

function addUser(user: User & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', user.email)
  formData.append('fullName', user.fullName)
  formData.append('gender', user.gender)
  formData.append('password', user.password)
  formData.append('roles', JSON.stringify(user.roles))
  formData.append('salary', user.salary.toString())
  formData.append('tel', user.tel)
  formData.append('position', user.position)
  formData.append('branch', user.branch)
  if (user.files && user.files.length > 0) formData.append('file', user.files[0])
  return http.post('/users', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateUser(user: User & { files: File[] }) {
  const formData = new FormData()
  formData.append('email', user.email)
  formData.append('fullName', user.fullName)
  formData.append('gender', user.gender)
  formData.append('password', user.password)
  formData.append('tel', user.tel)
  formData.append('position', user.position)
  formData.append('branch', user.branch)
  formData.append('salary', user.salary.toString())
  formData.append('roles', JSON.stringify(user.roles))
  if (user.files && user.files.length > 0) formData.append('file', user.files[0])
  return http.post(`/users/${user.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delUser(user: User) {
  return http.delete(`/users/${user.id}`)
}

function getUser(id: number) {
  return http.get(`/users/${id}`)
}

function getreportcheckwork(id: number) {
  return http.get(`/users/CountEntriesByYear/${id}`)
}

function getLowsalary() {
  return http.get(`/users/users/findlow`)
}

function getUseridbybranchid(id: number) {
  return http.get(`/users/Useridbybranchid/${id}`)
}

function getRateSalary(min: number, max: number, idbranch: number) {
  return http.get(`/users/users/fRateSalaryUser/${min}/${max}/${idbranch}`)
}

function getCountNameByBranchId(id: number) {
  return http.get(`/users/getCountNameByBranchId/${id}`)
}

function getUsers() {
  return http.get('/users')
}

export default {
  addUser,
  updateUser,
  delUser,
  getUser,
  getUsers,
  getLowsalary,
  getRateSalary,
  getUseridbybranchid,
  getreportcheckwork,
  getCountNameByBranchId
}
