import type { OrderIngredient } from '@/types/OrderIngredient'
import http from './http'

function formatDate(date: Date) {
  const year = date.getFullYear()
  const month = (date.getMonth() + 1).toString().padStart(2, '0')
  const day = date.getDate().toString().padStart(2, '0')
  return `${year}-${month}-${day}`
}

function addOrderIngredient(orderingredient: OrderIngredient & { files: File[] }) {
  const formData = new FormData()
  formData.append('id', orderingredient.id?.toString())
  formData.append('name', orderingredient.name)
  formData.append('price', orderingredient.price.toString())
  formData.append('amount', orderingredient.amount.toString())
  formData.append('Savedate', orderingredient.Savedate)
  if (orderingredient.files && orderingredient.files.length > 0)
    formData.append('file', orderingredient.files[0])
  console.log('before add')
  return http.post('/orderingredients', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateOrderIngredient(orderingredient: OrderIngredient & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', orderingredient.name)
  formData.append('price', orderingredient.price.toString())
  formData.append('amount', orderingredient.amount.toString())
  if (orderingredient.files && orderingredient.files.length > 0)
    formData.append('file', orderingredient.files[0])
  return http.post(`/orderingredients/${orderingredient.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delOrderIngredient(orderingredient: OrderIngredient) {
  return http.delete(`/orderingredients/${orderingredient.id}`)
}

function getOrderIngredient(id: number) {
  return http.get(`/orderingredients/${id}`)
}

function getOrderIngredients() {
  return http.get('/orderingredients')
}

export default {
  addOrderIngredient,
  updateOrderIngredient,
  delOrderIngredient,
  getOrderIngredient,
  getOrderIngredients
}
